use crate::assembly;
use std::convert::TryFrom;

#[derive(Debug,Clone,Copy,PartialEq)]
/// Used to refer to one of the four 16-bit [Register]s
pub enum RegisterLabel {A,B,C,D}

impl ToString for RegisterLabel {
    fn to_string(&self) -> String {
        String::from(match self {
            RegisterLabel::A => "A",
            RegisterLabel::B => "B",
            RegisterLabel::C => "C",
            RegisterLabel::D => "D",
        })
    }
}

/// Used to refer to any of the 8 logical [Register]s - A, B, C, and D, either the full 16 bits or the lower 8 bits
#[derive(Debug,Clone,Copy,PartialEq)]
pub struct Register {
    /// The [label](RegisterLabel) of the register - [A](RegisterLabel::A), [B](RegisterLabel::B), [C](RegisterLabel::C), or [D](RegisterLabel::D)
    pub label: RegisterLabel,
    /// if a register is `truncated`, it is taken to refer only to the lower 8 bits of the full register
    pub truncated: bool
}

/// const for conveniently referring to register A_8
pub const A_8: Register = Register{label: RegisterLabel::A, truncated: true};
/// const for conveniently referring to register B_8
pub const B_8: Register = Register{label: RegisterLabel::B, truncated: true};
/// const for conveniently referring to register C_8
pub const C_8: Register = Register{label: RegisterLabel::C, truncated: true};
/// const for conveniently referring to register D_8
pub const D_8: Register = Register{label: RegisterLabel::D, truncated: true};

/// const for conveniently referring to register A_16
pub const A_16: Register = Register{label: RegisterLabel::A, truncated: false};
/// const for conveniently referring to register B_16
pub const B_16: Register = Register{label: RegisterLabel::B, truncated: false};
/// const for conveniently referring to register C_16
pub const C_16: Register = Register{label: RegisterLabel::C, truncated: false};
/// const for conveniently referring to register D_16
pub const D_16: Register = Register{label: RegisterLabel::D, truncated: false};


impl From<u8> for Register {
    fn from(num: u8) -> Register {
        let truncated: bool = (num & 1) == 1; // if the last bit is set, then we are referring to the truncated register
        match (num >> 1) & 0x03 { // the other 2 bits denote which register to use
            0b00 => Register{label: RegisterLabel::A, truncated: truncated},
            0b01 => Register{label: RegisterLabel::B, truncated: truncated},
            0b10 => Register{label: RegisterLabel::C, truncated: truncated},
            0b11 => Register{label: RegisterLabel::D, truncated: truncated},
            _ => panic!("insanity error: could not get register from u8"), // This should literally never happen
        }
    }
}

impl Register {
    pub fn read_from(name: &str) -> Result<(Register, &str), assembly::Error> {
        let name = name.trim();
        match name {
            name if name.starts_with("A_8")  =>  Ok((A_8, &name[3..])),
            name if name.starts_with("B_8")  =>  Ok((B_8, &name[3..])),
            name if name.starts_with("C_8")  =>  Ok((C_8, &name[3..])),
            name if name.starts_with("D_8")  =>  Ok((D_8, &name[3..])),
            name if name.starts_with("A_16") => Ok((A_16, &name[4..])),
            name if name.starts_with("B_16") => Ok((B_16, &name[4..])),
            name if name.starts_with("C_16") => Ok((C_16, &name[4..])),
            name if name.starts_with("D_16") => Ok((D_16, &name[4..])),
            _ => Err(assembly::Error{
                kind: assembly::ErrorKind::BadRegister(String::from(name)),
                underlying: None
            })
        }
    }
}

impl ToString for Register {
    fn to_string(&self) -> String {
        format!("{label}_{size}", label=self.label.to_string(), size=if self.truncated {8} else {16})
    }
}

impl TryFrom<&str> for Register {
    type Error = assembly::Error;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        match s {
            "A_8"  =>  Ok(A_8),
            "B_8"  =>  Ok(B_8),
            "C_8"  =>  Ok(C_8),
            "D_8"  =>  Ok(D_8),
            "A_16" => Ok(A_16),
            "B_16" => Ok(B_16),
            "C_16" => Ok(C_16),
            "D_16" => Ok(D_16),
            _ => Err(assembly::Error{
                kind: assembly::ErrorKind::BadRegister(String::from(s)),
                underlying: None
            })
        }
    }
}

impl From<Register> for u8 {
    fn from(r: Register) -> u8 {
        match r.label {
            RegisterLabel::A => 0x0 | if r.truncated {1} else {0},
            RegisterLabel::B => 0x2 | if r.truncated {1} else {0},
            RegisterLabel::C => 0x4 | if r.truncated {1} else {0},
            RegisterLabel::D => 0x6 | if r.truncated {1} else {0},
        }
    }
}