use crate::common::Resolvable;
use crate::common::Register;
use crate::assembly;
use std::convert::TryFrom;

/// The instruction set, generalized to accept generic values for delayed resolution
#[derive(Debug, Copy, Clone)]
pub enum Instruction<T> {
    // MEMORY ACCESS
    /// Load the value obtained from the given [Resolvable] into the given [Register]
    LOD(Register, Resolvable<T>),
    /// Store the value in the given [Register] to the memory addressed by the value obtained from the given
    /// [Resolvable]
    STR(Register, Resolvable<T>),

    // operations
    /// Add the value obtained from the [Resolvable] to the value in the given [Register],
    /// storing the result in the register
    ADD(Register, Resolvable<T>),
    /// Multiply the value obtained from the [Resolvable] by the value in the given [Register],
    /// storing the result in the register
    MUL(Register, Resolvable<T>),
    /// Subtract the value obtained from the [Resolvable] from the value in the given [Register],
    /// storing the result in the register
    SUB(Register, Resolvable<T>),
    /// Divide the value obtained from the [Resolvable] by the value in the given [Register],
    /// storing the result in the register
    DIV(Register, Resolvable<T>),

    // exceptions
    /// If the last operation had an [error](crate::runtime::Error), move the error [flag](crate::runtime::flags) into the given [Register]. Otherwise set this register to zero
    MVF(Register),

    // flow control
    /// On the next cycle, execute the instruction at the memory addressed by the value obtained by the given [Resolvable] instead of the next instruction
    JMP(Resolvable<T>),           // Set the instruction pointer to the given address
    /// If the value at the given [Register] is zero, then [JMP](self::Instruction::JMP) to the given [Resolvable]; otherwise progress to the next instruction as normal
    BRZ(Register, Resolvable<T>),
    /// If the value at the given [Register] is *not* zero, then [JMP](self::Instruction::JMP) to the given [Resolvable]; otherwise progress to the next instruction as normal
    BNZ(Register, Resolvable<T>), // Set the instruction pointer to the value obtained from the given [Resolvable] if the given [Register] is not equal to zero
    /// Halt the machine; after this, further [cycle](crate::runtime::RUSTIC::cycle)s will have no effect
    HLT,                    // Halt execution

    // stack manipulation
    /// Push the value in the given [Register] to the address pointed to by the stack pointer, and increment the stack pointer
    /// by the size of the pushed value (1 byte if [truncated](Register::truncated), otherwise 2)
    PSH(Register),
    /// Load the value at the address pointed to by the stack pointer into the given [Register], and decrement the stack pointer
    /// by the size of the pushed value (1 byte if [truncated](Register::truncated), otherwise 2)
    POP(Register),
    /// Push the address of the next instruction onto the stack and set the instruction pointer to the value obtained from the given [Resolvable]
    CAL(Resolvable<T>),
    /// Pop a 2-byte address off the stack to execute on the next cycle instead of executing the next instruction
    RET
}

impl<T> Instruction<T> {
    /// The number of bytes by which the instruction pointer advances by default when this instruction is executed;
    /// this may be different depending on the effects of the instruction
    pub const fn bytes(&self) -> u8 {
        match self {
            Instruction::LOD(_,_) => 4, // (opcode)1 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::STR(_,_) => 4, // (opcode)2 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::ADD(_,_) => 4, // (opcode)01 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::MUL(_,_) => 4, // (opcode)02 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::SUB(_,_) => 4, // (opcode)03 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::DIV(_,_) => 4, // (opcode)04 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::MVF(_)   => 1, // (opcode)3 (register)_
            Instruction::JMP(_)   => 0, // (opcode)08 (resolvableindicator)__ (resolvable)__ __
            Instruction::BRZ(_,_) => 0, // (opcode)4 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::BNZ(_,_) => 0, // (opcode)5 (register)_ (resolvableindicator)__ (resolvable)__ __
            Instruction::HLT      => 0, // (opcode)00
            Instruction::PSH(_)   => 1, // (opcode)6 (register)_
            Instruction::POP(_)   => 1, // (opcode)7 (register)_
            Instruction::CAL(_)   => 0, // (opcode)05 (resolvableindicator)__ (resolvable)__ __
            Instruction::RET      => 0  // (opcode)06
        }
    }

    /// The number of bytes taken up by this instruction in memory
    pub const fn bytes_taken(&self) -> u8 {
        match self {
            Instruction::LOD(_,_) => 4,
            Instruction::STR(_,_) => 4,
            Instruction::ADD(_,_) => 4,
            Instruction::MUL(_,_) => 4,
            Instruction::SUB(_,_) => 4,
            Instruction::DIV(_,_) => 4,
            Instruction::MVF(_)   => 1,
            Instruction::JMP(_)   => 4,
            Instruction::BRZ(_,_) => 4,
            Instruction::BNZ(_,_) => 4,
            Instruction::HLT      => 1,
            Instruction::PSH(_)   => 1,
            Instruction::POP(_)   => 1,
            Instruction::CAL(_)   => 4,
            Instruction::RET      => 1
        }
    }
}

#[derive(Debug,PartialEq)]
pub enum InstructionType {
    LOD,
    STR,
    ADD,
    MUL,
    SUB,
    DIV,
    MVF,
    JMP,
    BRZ,
    BNZ,
    HLT,
    PSH,
    POP,
    CAL,
    RET
}

impl ToString for InstructionType {
    fn to_string(&self) -> String {
        String::from(match self {
            InstructionType::LOD => "LOD",
            InstructionType::STR => "STR",
            InstructionType::ADD => "ADD",
            InstructionType::MUL => "MUL",
            InstructionType::SUB => "SUB",
            InstructionType::DIV => "DIV",
            InstructionType::MVF => "MVF",
            InstructionType::JMP => "JMP",
            InstructionType::BRZ => "BRZ",
            InstructionType::BNZ => "BNZ",
            InstructionType::HLT => "HLT",
            InstructionType::PSH => "PSH",
            InstructionType::POP => "POP",
            InstructionType::CAL => "CAL",
            InstructionType::RET => "RET"
        })
    }
}

impl TryFrom<&str> for InstructionType {
    type Error = assembly::Error;

    fn try_from(s: &str) -> Result<InstructionType, Self::Error> {
        match s {
            "LOD" => Ok(InstructionType::LOD),
            "STR" => Ok(InstructionType::STR),
            "ADD" => Ok(InstructionType::ADD),
            "MUL" => Ok(InstructionType::MUL),
            "SUB" => Ok(InstructionType::SUB),
            "DIV" => Ok(InstructionType::DIV),
            "MVF" => Ok(InstructionType::MVF),
            "JMP" => Ok(InstructionType::JMP),
            "BRZ" => Ok(InstructionType::BRZ),
            "BNZ" => Ok(InstructionType::BNZ),
            "HLT" => Ok(InstructionType::HLT),
            "PSH" => Ok(InstructionType::PSH),
            "POP" => Ok(InstructionType::POP),
            "CAL" => Ok(InstructionType::CAL),
            "RET" => Ok(InstructionType::RET),
            _ => Err(assembly::Error{
                kind: assembly::ErrorKind::BadInstruction(String::from(s)),
                underlying: None
            })
        }
    }
}