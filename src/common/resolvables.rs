use crate::common::Register;
use crate::assembly;

#[derive(Debug, Copy, Clone)]
/// Represents a value which "resolves" to a T - in particular, at runtime, it can be [resolved into a u16 value](crate::runtime::RUSTIC::resolve).
/// It is declared here generically to allow delayed conversion from T to u16 during e.g. the assembly process.
pub enum Resolvable<T> {
    /// Resolves to the value held in the given register
    Register(Register),
    /// Resolves to the value held at the given location, offset by the value at the given register
    MemoryLocation(MemoryLocation<T>),
    /// Resolves to the given literal value
    Literal(T)
}

impl<T: ToString> ToString for Resolvable<T> {
    fn to_string(&self) -> String {
        match self {
            Resolvable::Literal(l) => l.to_string(),
            Resolvable::MemoryLocation(m) => m.to_string(),
            Resolvable::Register(r) => r.to_string()
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct MemoryLocation<T> {
    pub base_location: T,
    pub offset: Offset
}

impl<T: ToString> ToString for MemoryLocation<T> {
    fn to_string(&self) -> String {
        format!("${}{}", self.base_location.to_string(), self.offset.to_string())
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Offset(pub Option<Register>);

impl ToString for Offset {
    fn to_string(&self) -> String {
        let Offset(o) = self;
        match o {
            Some(r) => format!("[{}]", r.to_string()),
            None => String::from("")
        }
    }
}

impl Offset {
    pub fn read_from(line: &str) -> Result<(Offset, &str), assembly::Error> {
        line.chars().next()
        .and_then(|c| if c == '[' {Some(())} else {None})
        .map_or(
            Ok((Offset(None), line)),
            |_| Register::read_from(&line[1..])
                .and_then(|(reg, rest)| rest.chars().next().ok_or(assembly::Error{
                        kind: assembly::ErrorKind::BadOffset(String::from(rest)),
                        underlying: None
                    }).and_then(|c| if c == ']' {Ok(())} else {Err(assembly::Error{
                        kind: assembly::ErrorKind::BadOffset(String::from(rest)),
                        underlying: None
                    })}).and(Ok((Offset(Some(reg)), &rest[1..])))
                )
        )
    }
}