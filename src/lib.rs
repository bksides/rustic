/// A crate containing binaries and libraries related to the RUSTIC-16 fantasy console

// declare some helpful macros that are used in the submodules
macro_rules! unwrap_or_return {
    ($e:expr) => {
        match $e {
            Ok(res) => res,
            Err(err) => return Err(err)
        }
    };
}

/// Contains elements common to multiple submodules
pub mod common;
/// Contains elements pertaining to assembling programs written in RUSTIC assembly
pub mod assembly;
/// Contains elements pertaining to running a RUSTIC instance
pub mod runtime;
/// Contains the lexer
pub mod lexer;

#[cfg(test)]
pub mod tests {
    use crate::runtime::{RUSTIC, Instruction, Resolvable};
    use crate::common::{A_16, A_8};

    #[test]
    fn exec_should_execute_lod() {
        let mut machine = RUSTIC::new();
        let instr = Instruction::LOD(A_16, Resolvable::Literal(0xBEEF));
        machine.exec(instr).unwrap();
        assert_eq!(machine.at_register(A_16), 0xBEEF);
        
        let instr = Instruction::LOD(A_8, Resolvable::Literal(0xDEAD));
        machine.exec(instr).unwrap();
        assert_eq!(machine.at_register(A_16), 0xBEAD);
    }
}