# RUSTIC Runtime

The RUSTIC runtime implements the **RUSTIC computation model** - the logical
structure of the specified computer that the [RUSTIC Instruction Set] runs on.
That model is described here.

In broad terms, the model describes a computer with:
- a 16-bit word size
- four 16-bit registers (each of whose lower 8 bits can be manipulated independently)
- 64KiB of logical memory containing
    - the program running in memory
    - any static data contained in the program
    - a LIFO stack for use by the program
    - a free-for-all heap for use by the program
- a "`cycling`" mechanism which cycles over and executes instructions in memory sequentially

We will elaborate on all these components as follows:

- [Boot Process](#boot-process)
- [Instructions](#instructions)
- [Registers](#registers)
- [Memory](#memory)
- [Stack](#stack)
- [Resolvables](#resolvables)

## Boot and Execution Process

In order to run a program, the RUSTIC runtime must load it into memory.  It does this
starting at address zero, so that the addresses hardcoded into the program aren't offset.

It then prepares to execute the instruction at address 0, and creates a stack of
undefined size starting at the first address in memory *not* taken up by the loaded program;
this stack will grow upwards (as a result, dynamically allocated memory should prefer the
upper region of the address space).  If the machine is halted, it ceases to be halted.

The runtime then begins cycling over instructions, executing them one-by-one.  Usually, instructions
are executed in the order in which they're laid out in memory, although certain instructions
can change this behavior as detailed below.

## Instructions

Each cycle, the runtime reads bytes from memory and attempts to interpret them as instructions.
It does this by reading the first byte - the **opcode** - and identifying the instruction.  Then, knowing the
instruction, it also knows how many total bytes to read to interpret as operands to the instruction.

| first byte | instruction | total bytes |
|------------|-------------|-------------|
| 0x1X       | LOD         | 4           |
| 0x2X       | STR         | 4           |
| 0x3X       | MVF         | 1           |
| 0x4X       | BRZ         | 4           |
| 0x5X       | BNZ         | 4           |
| 0x6X       | PSH         | 1           |
| 0x7X       | POP         | 1           |
| 0x8X       | ADD         | 4           |
| 0x9X       | MUL         | 4           |
| 0xAX       | SUB         | 4           |
| 0xBX       | DIV         | 4           |
| 0x01       | JMP         | 4           |
| 0x02       | CAL         | 4           |
| 0x03       | RET         | 1           |
| 0x00       | HLT         | 1           |

The remaining bytes encode operands - the details of the encoding is described separately for each operand type.

## Registers

## Memory

## Resolvables