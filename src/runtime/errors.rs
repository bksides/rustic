use crate::common::Register;
use crate::runtime::Resolvable;

/// Flag values associated with corresponding error conditions
pub mod flags {
    pub const FAILED_MEMORY_READ: u8 = 1;
    pub const INVALID_REGISTER: u8 = 2;
    pub const BAD_INSTRUCTION: u8 = 3;
    pub const FAILED_MEMORY_WRITE: u8 = 4;
    pub const REGISTER_OVERFLOW: u8 = 5;
    pub const DIVIDE_BY_ZERO: u8 = 6;
    pub const BAD_RESOLVABLE: u8 = 7;
}

#[derive(Debug)]
/// Represents a runtime error
pub enum Error {
    /// Indicates that a memory read failed at the given address
    FailedMemoryRead(u16),
    /// Indicates that the given byte does not represent a valid register
    InvalidRegister(u8),
    /// Indicates that the given byte does not represent a valid instruction
    BadInstruction(u8),
    /// Indicates that a memory write failed at the given address
    FailedMemoryWrite(u16),
    /// Indicates that the given [Register](crate::common::Register) overflowed
    RegisterOverflow(Register),
    /// Indicates that a division was attempted by the given [Resolvable](crate::runtime::Resolvable), which evaluated to zero
    DivideByZero(Resolvable),
    /// Indicates that the given byte could not be interpreted as part of a [Resolvable](crate::runtime::Resolvable)
    BadResolvable(u8),
}

impl Error {
    /// Returns the internal [flag](self::flags) value associated with this runtime error
    pub fn flag(&self) -> u8 {
        match self {
            Error::FailedMemoryRead(_) => flags::FAILED_MEMORY_READ,
            Error::InvalidRegister(_) => flags::INVALID_REGISTER,
            Error::BadInstruction(_) => flags::BAD_INSTRUCTION,
            Error::FailedMemoryWrite(_) => flags::FAILED_MEMORY_WRITE,
            Error::RegisterOverflow(_) => flags::REGISTER_OVERFLOW,
            Error::DivideByZero(_) => flags::DIVIDE_BY_ZERO,
            Error::BadResolvable(_) => flags::BAD_RESOLVABLE,
        }
    }
}

impl ToString for Error {
    fn to_string(&self) -> String {
        match self {
            Error::FailedMemoryRead(addr) => format!("failed memory read at address {}", addr),
            Error::InvalidRegister(register) => format!("invalid register: {}", register),
            Error::BadInstruction(instruction) => format!("bad instruction: {}", instruction),
            Error::FailedMemoryWrite(addr) => format!("failed memory write: {}", addr),
            Error::RegisterOverflow(register) => format!("register overflow: {}", register.to_string()),
            Error::DivideByZero(resolvable) => format!("division by zero: dividend {}=0", resolvable.to_string()),
            Error::BadResolvable(resolvable) => format!("bad resolvable indicator: {}", resolvable),
        }
    }
}