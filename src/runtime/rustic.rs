use crate::common::MemoryLocation;
use crate::common::Offset;
use crate::runtime::{Error, Instruction, Resolvable};
use crate::common::{Register, RegisterLabel};

/// Represents an instance of the RUSTIC fantasy console
pub struct RUSTIC {
    halted:                 bool,           // set to *true* if the machine has encountered a HLT instruction; cycling the machine while halted is true will always
                                            // be a NOP
    register_a:             u16,            // Registers A, B, and C can hold a single i64 each at any given time.
    register_b:             u16,
    register_c:             u16,
    register_d:             u16,
    instruction_pointer:    u16,            // The instruction pointer is a special register
                                            // at the end of each computation cycle, the value at the instruction pointer is used to fetch the next
                                            // instruction
    stack_ptr:              u16,            // points to the current top of the stack
    flag:                   u8,             // The flag register will be set if the CPU encountered an exceptional circumstance during the previous operation 
    memory:                 [u8; 1 << 15],  // This is RUSTIC's internal memory store.  It's mapped to addresses 0x0000-0xEFFF
                                            // The remaining address space is reserved for interfacing with peripherals (to be implemented)
}

impl RUSTIC {
    /// Cycle the machine, executing a single instruction and preparing to execute the next
    pub fn cycle(&mut self) -> Result<(), Error> {
        if self.halted {
            return Ok(());
        }

        let instruction_result = Instruction::from_bytes(&self.memory, self.instruction_pointer);

        match instruction_result {
            Err(runtime_error) => {
                self.flag = runtime_error.flag();
                self.halted = true;
                Err(runtime_error)
            },
            Ok(instruction) => {
                self.instruction_pointer += u16::from(instruction.bytes());
                self.exec(instruction)
            }
        }
    }

    /// Returns `true` if the machine is halted
    pub fn is_halted(&self) -> bool {
        return self.halted;
    }

    /// Resolve the given resolvable to a u8 value
    pub fn resolve_u8(&self, resolvable: Resolvable) -> Result<u8, Error> {
        match resolvable {
            Resolvable::Literal(val) => Ok(val as u8),
            Resolvable::MemoryLocation(
                MemoryLocation{
                    base_location: addr,
                    offset: Offset(off)
                }) => self.fetch_from_addr(addr + if let Some(reg) = off {self.at_register(reg)} else {0}),
            Resolvable::Register(reg) => Ok(self.at_register(reg) as u8)
        }
    }

    /// Resolve the given resolvable to a u16 value
    pub fn resolve(&self, resolvable: Resolvable) -> Result<u16, Error> {
        match resolvable {
            Resolvable::Literal(val) => Ok(val),
            Resolvable::MemoryLocation(
                MemoryLocation{
                    base_location: addr,
                    offset: Offset(off)
                }) => self.fetch_from_addr_u16(addr + if let Some(reg) = off {self.at_register(reg)} else {0}),
            Resolvable::Register(reg) => Ok(self.at_register(reg))
        }
    }

    /// Execute the given instruction
    pub fn exec(&mut self, instr: Instruction) -> Result<(), Error> {
        match instr {
            Instruction::LOD(register, resolvable) => match if register.truncated {self.resolve_u8(resolvable).map(u16::from)} else {self.resolve(resolvable)} {
                Ok(val) => {
                    println!("{}", val);
                    self.set_register(register, val);
                    Ok(())
                },
                Err(err) => Err(err),
            },
            Instruction::STR(register, resolvable) => self.resolve(resolvable).and_then(|memaddr| if register.truncated {
                self.store_to_addr(memaddr, self.at_register(register) as u8)
            } else {
                self.store_to_addr_u16(memaddr, self.at_register(register))
            }),
            Instruction::MVF(register) => {
                self.set_register(register, self.flag as u16);
                Ok(())
            },
            Instruction::JMP(memaddr) => {
                self.instruction_pointer = unwrap_or_return!(self.resolve(memaddr));
                Ok(())
            },
            Instruction::BRZ(register, memaddr) => {
                if self.at_register(register) == 0 {
                    self.instruction_pointer = unwrap_or_return!(self.resolve(memaddr));
                } else {
                    self.instruction_pointer += instr.bytes_taken() as u16;
                }
                Ok(())
            },
            Instruction::BNZ(register, memaddr) => {
                if self.at_register(register) != 0 {
                    self.instruction_pointer = unwrap_or_return!(self.resolve(memaddr));
                } else {
                    self.instruction_pointer += instr.bytes_taken() as u16;
                }
                Ok(())
            },
            Instruction::PSH(register) => if register.truncated {
                self.store_to_addr(self.stack_ptr as u16, self.at_register(register) as u8).and_then(|_| {
                    self.stack_ptr += 1;
                    Ok(())
                })
            } else {
                self.store_to_addr_u16(self.stack_ptr as u16, self.at_register(register)).and_then(|_| {
                    self.stack_ptr += 2;
                    Ok(())
                })
            },
            Instruction::POP(register) => if register.truncated {
                self.fetch_from_addr((self.stack_ptr-1) as u16)
                .and_then(|val| {
                    self.set_register(register, val as u16);
                    self.stack_ptr-=1;
                    Ok(())
                })
            } else {
                self.fetch_from_addr_u16((self.stack_ptr-2) as u16)
                .and_then(|val| {
                    self.set_register(register, val);
                    self.stack_ptr-=2;
                    Ok(())
                })
            },
            Instruction::HLT => {
                self.halted = true;
                Ok(())
            },
            Instruction::ADD(register, resolvable) => {
                let addend = unwrap_or_return!(if register.truncated {self.resolve_u8(resolvable).map(u16::from)} else {self.resolve(resolvable)});
                let (result, overflow) = self.at_register(register).overflowing_add(addend);
                self.set_register(register, result);
                if overflow || register.truncated && result > std::u8::MAX as u16 {
                    Err(Error::RegisterOverflow(register))
                } else {
                    Ok(())
                }
            },
            Instruction::MUL(register, resolvable) => {
                let factor = unwrap_or_return!(if register.truncated {self.resolve_u8(resolvable).map(u16::from)} else {self.resolve(resolvable)});
                let (result, overflow) = self.at_register(register).overflowing_mul(factor);
                self.set_register(register, result);
                if overflow || register.truncated && result > std::u8::MAX as u16 {
                    Err(Error::RegisterOverflow(register))
                } else {
                    Ok(())
                }
            },
            Instruction::DIV(register, resolvable) => {
                let divisor = unwrap_or_return!(if register.truncated {self.resolve_u8(resolvable).map(u16::from)} else {self.resolve(resolvable)});
                match self.at_register(register).checked_div(divisor) {
                    None => Err(Error::DivideByZero(resolvable)),
                    Some(answer) => {
                        self.set_register(register, answer);
                        Ok(())
                    }
                }
            },
            Instruction::SUB(register, resolvable) => {
                let subtractor = unwrap_or_return!(if register.truncated {self.resolve_u8(resolvable).map(u16::from)} else {self.resolve(resolvable)});
                let (result, overflow) = self.at_register(register).overflowing_sub(subtractor);
                self.set_register(register, result);
                if overflow {
                    Err(Error::RegisterOverflow(register))
                } else {
                    Ok(())
                }
            },
            Instruction::CAL(resolvable) => self.resolve(resolvable)
            .and_then(|addr|self.store_to_addr_u16(self.stack_ptr as u16, self.instruction_pointer + instr.bytes_taken() as u16)
                .and_then(|_| {
                    self.stack_ptr += 2;
                    self.instruction_pointer = addr;
                    Ok(())
                })),
            Instruction::RET => self.fetch_from_addr_u16((self.stack_ptr-2) as u16)
                .and_then(|ptr| {
                    self.stack_ptr -= 2;
                    self.instruction_pointer = ptr;
                    Ok(())
                }),
        }.or_else(|err| {
            self.flag = err.flag();
            Err(err)
        })
    }

    /// Returns the value at the given register
    pub fn at_register(&self, reg: Register) -> u16 {
        match reg.label {
            RegisterLabel::A => if reg.truncated {self.register_a & 0xFF} else {self.register_a},
            RegisterLabel::B => if reg.truncated {self.register_b & 0xFF} else {self.register_b},
            RegisterLabel::C => if reg.truncated {self.register_c & 0xFF} else {self.register_c},
            RegisterLabel::D => if reg.truncated {self.register_d & 0xFF} else {self.register_d}
        }
    }

    /// Returns the value of the instruction pointer
    pub fn instruction_ptr(&self) -> u16 {
        self.instruction_pointer
    }

    /// Sets the value of the given register
    pub fn set_register(&mut self, reg: Register, val: u16) {
        match reg.label {
            RegisterLabel::A => if reg.truncated {self.register_a = (self.register_a & 0xFF00) | (val & 0xFF)} else {self.register_a = val},
            RegisterLabel::B => if reg.truncated {self.register_b = (self.register_b & 0xFF00) | (val & 0xFF)} else {self.register_b = val},
            RegisterLabel::C => if reg.truncated {self.register_c = (self.register_c & 0xFF00) | (val & 0xFF)} else {self.register_c = val},
            RegisterLabel::D => if reg.truncated {self.register_d = (self.register_d & 0xFF00) | (val & 0xFF)} else {self.register_d = val},
        }
    }

    /// Fetch a u8 value from the given byte
    pub fn fetch_from_addr(&self, ptr: u16) -> Result<u8, Error> {
        if ptr <= 0xEFFF {
            return Ok(self.memory[ptr as usize]);
        }
        return Err(Error::FailedMemoryRead(ptr));
    }

    /// Fetch a big-endian u16 value beginning at the given byte
    fn fetch_from_addr_u16(&self, ptr: u16) -> Result<u16, Error> {
        println!("Fetching from addr {}", ptr);
        if ptr <= 0xEFFE {
            let bytes: [u8; 2] = [self.memory[usize::from(ptr)], self.memory[usize::from(ptr+1)]];
            return Ok(u16::from_be_bytes(bytes));
        }
        return Err(Error::FailedMemoryRead(ptr));
    }

    /// Store a u8 value to the given byte
    fn store_to_addr(&mut self, ptr: u16, val: u8) -> Result<(), Error> {
        if ptr <= 0xEFFF {
            self.memory[usize::from(ptr)] = val;
            return Ok(());
        } else {
            return Err(Error::FailedMemoryWrite(ptr));
        }
    }

    /// Store a big-endian u16 value beginning at the given byte
    fn store_to_addr_u16(&mut self, ptr: u16, val: u16) -> Result<(), Error> {
        if ptr <= 0xEFFE {
            let bytes = val.to_be_bytes();
            self.memory[usize::from(ptr)] = bytes[0];
            self.memory[usize::from(ptr+1)] = bytes[1]; 
            return Ok(());
        } else {
            return Err(Error::FailedMemoryWrite(ptr));
        }
    }

    /// Return a newly initialized RUSTIC instance
    pub fn new() -> RUSTIC {
        RUSTIC{
            halted: false,
            register_a: 0,
            register_b: 0,
            register_c: 0,
            register_d: 0,
            instruction_pointer: 0,
            stack_ptr: 0,
            flag: 0,
            memory: [0; 1 << 15]
        }
    }

    /// Load the content of the given byte stream into memory
    pub fn load_into_memory<I: IntoIterator<Item=u8>>(&mut self, bytes: I) {
        let mut idx = 0;
        for byte in bytes {
            if idx >= 1 << 15 {
                break;
            }
            self.memory[idx] = byte;
            idx += 1;
        }
        self.stack_ptr = idx as u16;
        self.instruction_pointer = 0;
        self.halted = false;
    }

    /// Returns the current value of the flag
    pub fn flag(&self) -> u8 {
        self.flag
    }
}