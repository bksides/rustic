use crate::common::Offset;

use crate::common;
use crate::common::Register;
use crate::runtime::Error;
use crate::common::MemoryLocation;

/// A type that can resolve to a u16 at runtime through the [resolve](crate::runtime::RUSTIC::resolve) function
pub type Resolvable = common::Resolvable<u16>;

impl Resolvable {
    /// Interpret the indicated bytes as a Resolvable
    pub fn from_bytes(bytes: &[u8], ptr: u16) -> Result<Resolvable, Error> {
        let indicator = unwrap_or_return!(bytes.get(ptr as usize).ok_or(Error::FailedMemoryRead(ptr)));
        match indicator {
            0x00 => {
                let bytes = [
                    unwrap_or_return!(bytes.get(ptr as usize + 1).ok_or(Error::FailedMemoryRead(ptr+1))),
                    unwrap_or_return!(bytes.get(ptr as usize + 2).ok_or(Error::FailedMemoryRead(ptr+2))),
                ];
                Ok(Resolvable::Literal(u16::from_be_bytes([*bytes[0], *bytes[1]])))
            },
            n if n & 0x08 == 0x08 => {
                let bytes = [
                    *unwrap_or_return!(bytes.get(ptr as usize + 1).ok_or(Error::FailedMemoryRead(ptr+1))),
                    *unwrap_or_return!(bytes.get(ptr as usize + 2).ok_or(Error::FailedMemoryRead(ptr+2))),
                ];
                let address = u16::from_be_bytes(bytes);
                Ok(Resolvable::MemoryLocation(MemoryLocation{base_location: address, offset: Offset(Some(Register::from(*n)))}))
            },
            n if n & 0x07 != 0 => {
                Ok(Resolvable::Register(Register::from(*n)))
            }
            n if n & 0x10 == 0x10 => {
                let bytes = [
                    *unwrap_or_return!(bytes.get(ptr as usize + 1).ok_or(Error::FailedMemoryRead(ptr+1))),
                    *unwrap_or_return!(bytes.get(ptr as usize + 2).ok_or(Error::FailedMemoryRead(ptr+2))),
                ];
                let address = u16::from_be_bytes(bytes);
                Ok(Resolvable::MemoryLocation(MemoryLocation{base_location: address, offset: Offset(Some(Register::from(*n)))}))
            }
            _ => Err(Error::BadResolvable(*indicator))
        }
    }

    /// Compile this Resolvable into bytes
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            Resolvable::Literal(literal) => {
                let bytes = literal.to_be_bytes();
                vec![0x00, bytes[0], bytes[1]]
            },
            Resolvable::MemoryLocation(MemoryLocation{base_location: address, offset: Offset(optional_reg)}) => {
                let bytes = address.to_be_bytes();
                vec![match optional_reg {
                    Some(reg) => 0x08 | u8::from(*reg),
                    None => 0x10
                }, bytes[0], bytes[1]]
            },
            Resolvable::Register(register) => vec![u8::from(*register), 0x00, 0x00]
        }
    }
}
