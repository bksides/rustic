use crate::common::Register;
use crate::common;
use crate::runtime::Error;
use crate::runtime::Resolvable;

/// simple u16-addressed instructions for use at runtime
pub type Instruction = common::Instruction<u16>;

/// The opcode of an instruction specifies its first half-byte.
/// Used to identify an instruction as soon as the first byte is read
pub mod opcodes {
    pub const LOD: u8 = 0x10;
    pub const STR: u8 = 0x20;
    pub const MVF: u8 = 0x30;
    pub const BRZ: u8 = 0x40;
    pub const BNZ: u8 = 0x50;
    pub const PSH: u8 = 0x60;
    pub const POP: u8 = 0x70;
    pub const ADD: u8 = 0x80;
    pub const MUL: u8 = 0x90;
    pub const SUB: u8 = 0xA0;
    pub const DIV: u8 = 0xB0;

    pub const HLT: u8 = 0x00;
    pub const JMP: u8 = 0x01;
    pub const CAL: u8 = 0x02;
    pub const RET: u8 = 0x03;
}

impl From<Instruction> for Vec<u8> {
    fn from(instruction: Instruction) -> Vec<u8> {
        match instruction {
            Instruction::LOD(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::LOD | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::STR(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::STR | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::ADD(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::ADD | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::MUL(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::MUL | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::SUB(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::SUB | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::DIV(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::DIV | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::MVF(r) => vec![opcodes::MVF | u8::from(r)],
            Instruction::JMP(res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::JMP, res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::BRZ(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::BRZ | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::BNZ(reg, res) => {
                let res_bytes = res.to_bytes();
                vec![opcodes::BNZ | u8::from(reg), res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::HLT => vec![opcodes::HLT],
            Instruction::PSH(r) => vec![opcodes::PSH | u8::from(r)],
            Instruction::POP(r) => vec![opcodes::POP | u8::from(r)],
            Instruction::CAL(m) => {
                let res_bytes = m.to_bytes();
                vec![opcodes::CAL, res_bytes[0], res_bytes[1], res_bytes[2]]
            },
            Instruction::RET => vec![opcodes::RET]
        }
    }
}

impl Instruction {
    /// Attempt to interpret the indicated bytes as an instruction
    pub fn from_bytes(bytes: &[u8], ptr: u16) -> Result<Instruction, Error> {
        bytes.get(ptr as usize)
            .ok_or(Error::FailedMemoryRead(ptr))
        .and_then(|&byte| match byte {
            opcodes::HLT => Ok(Instruction::HLT),
            opcodes::JMP => Resolvable::from_bytes(bytes, ptr+1).map(Instruction::JMP),
            opcodes::CAL => Resolvable::from_bytes(bytes, ptr+1).map(Instruction::CAL),
            opcodes::RET => Ok(Instruction::RET),
            _ => match byte & 0xF0 { // if none of the others match exactly, then just match the first 4 bits for the remaining opcodes
                opcodes::LOD => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::LOD(Register::from(byte), res)),
                opcodes::STR => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::STR(Register::from(byte), res)),
                opcodes::ADD => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::ADD(Register::from(byte), res)),
                opcodes::MUL => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::MUL(Register::from(byte), res)),
                opcodes::SUB => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::SUB(Register::from(byte), res)),
                opcodes::DIV => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::DIV(Register::from(byte), res)),
                opcodes::MVF => Ok(Instruction::MVF(Register::from(byte))),
                opcodes::BRZ => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::BRZ(Register::from(byte), res)),
                opcodes::BNZ => Resolvable::from_bytes(bytes, ptr+1).map(|res| Instruction::DIV(Register::from(byte), res)),
                opcodes::PSH => Ok(Instruction::PSH(Register::from(byte))),
                opcodes::POP => Ok(Instruction::POP(Register::from(byte))),
                _ => Err(Error::BadInstruction(byte))
            }
        })
    }
}