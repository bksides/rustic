use std::iter::Peekable;
use std::marker;

pub trait Token: Sized {
    type Error;

    fn read_from<I: Iterator<Item=char>>(iter: &mut Peekable<I>) -> Result<Self, Self::Error>;
}

// custom iterator
pub struct PeekingTakeWhile<'a, I: Iterator, F: FnMut(&I::Item) -> bool> {
    underlying: &'a mut Peekable<I>,
    pred: F
}

impl<'a, I: Iterator, F: FnMut(&I::Item) -> bool> Iterator for PeekingTakeWhile<'a, I, F> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        let PeekingTakeWhile{
            underlying,
            pred
        } = self;
        let potential_next = underlying.peek();
        match potential_next {
            Some(c) => if pred(c) {underlying.next()} else {None},
            None => None
        }
    }
}

pub fn peeking_take_while<'a, I: Iterator, F: FnMut(&I::Item) -> bool>(iter: &'a mut Peekable<I>, f: F) -> PeekingTakeWhile<'a, I, F> {
    PeekingTakeWhile{underlying: iter, pred: f}
}

pub struct TokenIter<I: Iterator<Item=char>, T: Token>(Peekable<I>, marker::PhantomData<T>);

impl<T: Token, I: Iterator<Item=char>> Iterator for TokenIter<I, T> {
    type Item = Result<T, T::Error>;

    fn next(&mut self) -> Option<Result<T, T::Error>> {
        let TokenIter(underlying, _) = self;
        if underlying.peek().is_some() {
            Some(T::read_from(underlying))
        } else {
            None
        }
    }
}
 
impl<T: Token, I: IntoIterator<Item=char>> From<I> for TokenIter<I::IntoIter,T> {
    fn from(iter: I) -> TokenIter<I::IntoIter,T> {
        TokenIter::<I::IntoIter,T>(iter.into_iter().peekable(), marker::PhantomData)
    }
}

pub trait SyntaxNode: Sized {
    type Token: Token;
    type Error;
    fn read_from<T: Iterator<Item=Self::Token>>(iter: Peekable<T>) -> Result<Self, Self::Error>;
}

#[test]
fn test_tokenization() {
    #[derive(Debug, PartialEq)]
    enum ExampleToken {
        Word(String),
        Number(u16),
        Space,
    }

    impl Token for ExampleToken {
        type Error = String;

        fn read_from<I: Iterator<Item=char>>(iter: &mut Peekable<I>) -> Result<Self, Self::Error> {
            match iter.peek() {
                Some(c) if c.is_ascii_alphabetic() => {
                    let token = peeking_take_while(iter, |c| c.is_ascii_alphabetic()).collect();
                    Ok(ExampleToken::Word(token))
                },
                Some(c) if c.is_ascii_digit() => {
                    let token: String = peeking_take_while(iter, |c| c.is_ascii_digit()).collect();
                    u16::from_str_radix(&token, 10)
                        .map(ExampleToken::Number)
                        .or(Err(String::from("bad number")))
                },
                Some(c) if c.is_ascii_whitespace() => {
                    while let Some(c) = iter.peek() {
                        if c.is_ascii_whitespace() {
                            iter.next();
                        } else {
                            break;
                        }
                    }
                    Ok(ExampleToken::Space)
                },
                Some(c) => Err(format!("unexpected character {}", c)),
                None => Err(String::from("no more characters to read"))
            }
        }
    }

    let token_iter: TokenIter<_, ExampleToken> = "hello\n  world\t 3327  beep1".chars().into();
    let mut correct_token_iter = vec![
        ExampleToken::Word(String::from("hello")),
        ExampleToken::Space,
        ExampleToken::Word(String::from("world")),
        ExampleToken::Space,
        ExampleToken::Number(3327),
        ExampleToken::Space,
        ExampleToken::Word(String::from("beep")),
        ExampleToken::Number(1)
    ].into_iter();
    for token_res in token_iter {
        assert_eq!(token_res.unwrap(), correct_token_iter.next().unwrap());
    }
    if let Some(token) = correct_token_iter.next() {
        panic!("Expected token: {:#?}", token);
    }
}