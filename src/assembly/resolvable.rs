use crate::common::Offset;
use std::collections::HashMap;

use crate::common;
use crate::common::{Register, MemoryLocation};
use crate::runtime;
use crate::assembly::{IndexedValue, Value, Literal, Error, ErrorKind};

/// compile-time stand-in for a u16 resolvable.  Allows the use of labels
/// to be converted into values at the end of compile-time.
pub type Resolvable = common::Resolvable<IndexedValue>;

impl Resolvable {
    /// Read a Resolvable from the given &[str]
    pub fn read_from(loc: &str) -> Result<(Resolvable, &str), Error> {
        Register::read_from(loc).map(|(reg, rest)| (Resolvable::Register(reg), rest))
            .or_else(|_| MemoryLocation::read_from(loc).map(|(mem, rest)| (Resolvable::MemoryLocation(mem), rest)))
            .or_else(|_| IndexedValue::read_from(loc).map(|(lit, rest)| (Resolvable::Literal(lit), rest)))
    }

    /// Compile this resolvable into raw bytes to be inserted into the executable
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<runtime::Resolvable, Error> {
        match self {
            Resolvable::Literal(l) => l.compile(labels).map(runtime::Resolvable::Literal),
            Resolvable::MemoryLocation(m) => m.compile(labels).map(runtime::Resolvable::MemoryLocation),
            Resolvable::Register(r) => Ok(runtime::Resolvable::Register(*r))
        }
    }
}

impl MemoryLocation<IndexedValue> {
    /// Read a [MemoryLocation]<[IndexedValue]> from the given &[str]
    pub fn read_from(line: &str) -> Result<(MemoryLocation<IndexedValue>, &str), Error> {
        let line = line.trim();
        line.chars().next().ok_or(Error{
            kind: ErrorKind::MemoryLocationExpected,
            underlying: None,
        }).and_then(|c| if c == '$' {Ok(())} else {Err(Error{
            kind: ErrorKind::BadMemoryLocation(String::from(line)),
            underlying: None,
        })}).and_then(|_| IndexedValue::read_from(&line[1..])
            .or_else(|err| match err.kind {
                ErrorKind::BadIndexedValue(_) => Ok((IndexedValue::NonIndexed(Value::Literal(Literal(0))), &line[1..])),
                _ => Err(err)
            }))
        .and_then(|(indexed_value, rest)| Offset::read_from(rest)
            .map(|(offset, rest)| (MemoryLocation{
                base_location: indexed_value,
                offset: offset
            }, rest))
        )
    }

    /// Compile this MemoryLocation<IndexedValue> into a MemoryLocation
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<MemoryLocation<u16>, Error> {
        self.base_location.compile(labels)
        .map(|loc| MemoryLocation{
            base_location: loc,
            offset: self.offset
        })
    }
}