use std::iter;
use std::convert::TryFrom;

use crate::lexer;
use crate::common::{Register, InstructionType, A_8};
use crate::assembly::{Error, ErrorKind};

#[derive(Debug, PartialEq)]
pub enum Token {
    Colon,
    Label(String),
    Register(Register),
    Number(u16),
    Instruction(InstructionType),
    OpenIndex,
    CloseIndex,
    Newline,
    DollarSign,
}

impl lexer::Token for Token {
    type Error = Error;

    fn read_from<I: Iterator<Item=char>>(iter: &mut iter::Peekable<I>) -> Result<Token, Error> {
        match iter.peek() {
            Some('\n') => {
                iter.next();
                Ok(Token::Newline)
            }
            Some('$') => {
                iter.next();
                Ok(Token::DollarSign)
            },
            Some('[') => {
                iter.next();
                Ok(Token::OpenIndex)
            },
            Some(']') => {
                iter.next();
                Ok(Token::CloseIndex)
            },
            Some(':') => {
                iter.next();
                Ok(Token::Colon)
            },
            Some('0') => {
                iter.next();
                match iter.peek() {
                    Some('x') => {
                        iter.next();
                        let token: String = lexer::peeking_take_while(iter, |c| c.is_ascii_hexdigit()).collect();
                        u16::from_str_radix(&token, 16)
                            .map(Token::Number)
                            .map_err(|err| Error{
                                kind: ErrorKind::BadLiteral(token),
                                underlying: Some(Box::new(err))
                            })

                    },
                    Some(d) if d.is_ascii_digit() => {
                        let token: String = lexer::peeking_take_while(iter, |c| c.is_ascii_digit()).collect();
                        u16::from_str_radix(&token, 10)
                            .map(Token::Number)
                            .map_err(|err| Error{
                                kind: ErrorKind::BadLiteral(token),
                                underlying: Some(Box::new(err))
                            })
                    },
                    _ => Ok(Token::Number(0))
                }
            },
            Some(c) if c.is_ascii_alphabetic() || *c == '_' => {
                let token: String = lexer::peeking_take_while(iter, |c| c.is_ascii_alphanumeric() || *c == '_').collect::<String>();
                Register::try_from(token.as_str())
                    .map(Token::Register)
                    .or_else(|_| InstructionType::try_from(token.as_str())
                        .map(Token::Instruction))
                    .or(Ok(Token::Label(token)))
            },
            Some(c) if c.is_ascii_digit() => {
                let token: String = lexer::peeking_take_while(iter, |c| c.is_ascii_digit()).collect();
                u16::from_str_radix(&token, 10).map_err(|err| Error{
                    kind: ErrorKind::BadLiteral(token),
                    underlying: Some(Box::new(err))
                }).map(Token::Number)
            },
            Some(c) if c.is_ascii_whitespace() => {
                iter.next();
                while let Some(c) = iter.peek() {
                    if c.is_ascii_whitespace() && *c != '\n' {
                        iter.next();
                    } else {
                        break;
                    }
                }
                Self::read_from(iter)
            },
            Some(c) => Err(Error{
                kind: ErrorKind::UnexpectedCharacter(*c),
                underlying: None
            }),
            None => Err(Error{
                kind: ErrorKind::EndOfIterator,
                underlying: None
            })
        }
    }
}

#[test]
fn test_assebly_tokens() {
    let tokens = lexer::TokenIter::<_, Token>::from("$DATA[40] \n LOD A_8 0x30 054 data[[] _LABEL_WORDS:\t A_84 LODER".chars());
    let mut correct_tokens = vec![
        Token::DollarSign,
        Token::Label(String::from("DATA")),
        Token::OpenIndex,
        Token::Number(40),
        Token::CloseIndex,
        Token::Newline,
        Token::Instruction(InstructionType::LOD),
        Token::Register(A_8),
        Token::Number(0x30),
        Token::Number(54),
        Token::Label(String::from("data")),
        Token::OpenIndex,
        Token::OpenIndex,
        Token::CloseIndex,
        Token::Label(String::from("_LABEL_WORDS")),
        Token::Colon,
        Token::Label(String::from("A_84")),
        Token::Label(String::from("LODER"))
    ].into_iter();
    for token_res in tokens {
        let token = token_res.unwrap_or_else(|err| panic!("bad token: {}", err.to_string()));
        let correct_token = correct_tokens.next().unwrap_or_else(|| panic!("unexpected token {:#?}", token));
        assert_eq!(token, correct_token);
    }
    if let Some(token) = correct_tokens.next() {
        panic!("Expected token: {:#?}", token);
    }
}