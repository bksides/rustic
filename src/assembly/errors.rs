/// Represents an error encountered during assembly
pub struct Error {
    /// An enum denoting the nature of the error
    pub kind: ErrorKind,
    /// An optional underlying error which caused this one
    pub underlying: Option<Box<dyn ToString>>
}

/// Convert the error to a string for display
impl ToString for Error {
    fn to_string(&self) -> String {
        self.kind.to_string() + &match &self.underlying {
            Some(err) => String::from("\n -> ") + &err.to_string(),
            None => String::new()
        }
    }
}

/// An enum denoting possible assembly-time errors
pub enum ErrorKind {
    /// The given string could not be interpreted as an [Instruction](crate::assembly::Instruction)
    BadInstruction(String),
    /// An [Instruction](crate::assembly::Instruction) was expected during parsing
    InstructionExpected,
    /// The given string could not be interpreted as a [Register](crate::common::Register)
    BadRegister(String),
    /// A [Register](crate::common::Register) was expected during parsing
    RegisterExpected,
    /// The given string could not be interpreted as an [indexed value](crate::assembly::MemoryLocation) (THIS VARIANT'S NAME WILL BE CHANGED)
    BadIndexedValue(String),
    /// The given string was used as a label, but is never declared (THIS VARIANT'S NAME WILL BE CHANGED)
    UnresolvedLabel(String),
    /// An [IndexedValue](crate::assembly::IndexedValue) was expected during parsing
    IndexedValueExpected,
    /// A [crate::assembly::Label] was expected during parsing
    LabelExpected,
    /// The given string could not be interpreted as a label
    BadLabel(String),
    /// The given string could not be interpreted as an offset
    BadOffset(String),
    /// A [crate::common::Offset] was expected during parsing
    OffsetExpected,
    /// A [crate::assembly::Resolvable] was expected during parsing
    ResolvableExpected,
    /// A [crate::assembly::MemoryLocation] was expected during parsing
    MemoryLocationExpected,
    /// The given string could not be interpreted as a memory location
    BadMemoryLocation(String),
    /// A literal was expected during parsing
    LiteralExpected,
    /// The given string could not be interpreted as data
    BadData(String),
    /// The given string could not be interpreted as a literal
    BadLiteral(String),
    /// The given string could not be interpreted as a label declaration
    BadLabelDeclaration(String),
    /// The given string was found after the line was expected to end
    EndOfLineExpected(String),
    /// A byte literal was expected during parsing
    ByteLiteralExpected,
    /// The input stream reached the end
    EndOfIterator,
    /// An unexpected character was encountered
    UnexpectedCharacter(char)
}

/// Describe the kind of error
impl ToString for ErrorKind {
    fn to_string(&self) -> String {
        match self {
            ErrorKind::ResolvableExpected      => String::from("expected resolvable"),
            ErrorKind::MemoryLocationExpected  => String::from("expected memory location"),
            ErrorKind::BadMemoryLocation(loc)  => format!("invalid memory location: {}", loc),
            ErrorKind::BadInstruction(name)    => format!("invalid instruction: {}", name),
            ErrorKind::InstructionExpected     => String::from("expected instruction"),
            ErrorKind::BadRegister(name)       => format!("invalid register: {}", name),
            ErrorKind::RegisterExpected        => String::from("expected register"),
            ErrorKind::BadIndexedValue(name)   => format!("invalid indexed value: {}", name),
            ErrorKind::IndexedValueExpected    => String::from("expected indexed value"),
            ErrorKind::LabelExpected           => String::from("expected label"),
            ErrorKind::BadLabel(name)          => format!("invalid label: {}",name),
            ErrorKind::UnresolvedLabel(label)  => format!("unresolved label: {}", label),
            ErrorKind::LiteralExpected         => String::from("expected literal"),
            ErrorKind::BadData(line)           => format!("bad data: {}", line),
            ErrorKind::BadLiteral(literal)     => format!("bad literal: {}", literal),
            ErrorKind::BadLabelDeclaration(d)  => format!("bad label declaration: {}", d),
            ErrorKind::EndOfLineExpected(rest) => format!("expected end of line: {}", rest),
            ErrorKind::BadOffset(ofst)         => format!("bad offset: {}", ofst),
            ErrorKind::OffsetExpected          => String::from("expected offset"),
            ErrorKind::ByteLiteralExpected     => String::from("expected byte literal"),
            ErrorKind::EndOfIterator           => String::from("end of char stream"),
            ErrorKind::UnexpectedCharacter(c)  => format!("unexpected character: '{}'", c),
        }
    }
}