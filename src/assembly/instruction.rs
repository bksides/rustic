use std::collections::HashMap;
use std::iter::Peekable;

use crate::assembly::{Error, ErrorKind, IndexedValue, Resolvable, Token};
use crate::common;
use crate::runtime;
use crate::common::{Register, InstructionType};
use crate::lexer::SyntaxNode;

/// [common::Instruction]s using [IndexedValue]s in place of [u16]s;
/// this allows instructions to reference [crate::assembly::Label]s which are later compiled
/// to addresses
pub type Instruction = common::Instruction<IndexedValue>;

impl SyntaxNode for Instruction {
    type Token = Token;
    type Error = Error;

    /// Used to try to read lines in the assembly language as Instruction<IndexedValue>s
    fn read_from<I: Iterator<Item=Token>>(iter: Peekable<I>) -> Result<Instruction, Error> {
        match iter.peek() {
            Some(Token::Instruction(i) (InstructionType::LOD) |
            (InstructionType::STR) |
            (InstructionType::BRZ) |
            (InstructionType::BNZ) |
            (InstructionType::ADD) |
            (InstructionType::SUB) |
            (InstructionType::MUL) |
            (InstructionType::DIV) => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::LOD(reg, res), rest))),
            "STR" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::STR(reg, res), rest))),
            "BRZ" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::BRZ(reg, res), rest))),
            "BNZ" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::BNZ(reg, res), rest))),
            "ADD" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::ADD(reg, res), rest))),
            "SUB" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::SUB(reg, res), rest))),
            "MUL" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::MUL(reg, res), rest))),
            "DIV" => Register::read_from(rest)
                .and_then(|(reg, rest)| Resolvable::read_from(rest)
                    .map(|(res, rest)| (Instruction::DIV(reg, res), rest))),
            "MVF" => Register::read_from(rest)
                .map(|(reg, rest)| (Instruction::MVF(reg), rest)),
            "PSH" => Register::read_from(rest)
                .map(|(reg, rest)| (Instruction::PSH(reg), rest)),
            "POP" => Register::read_from(rest)
                .map(|(reg, rest)| (Instruction::POP(reg), rest)),
            "JMP" => Resolvable::read_from(rest)
                .map(|(res, rest)| (Instruction::JMP(res), rest)),
            "CAL" => Resolvable::read_from(rest)
                .map(|(res, rest)| (Instruction::CAL(res), rest)),
            "HLT" => Ok((Instruction::HLT, rest)),
            "RET" => Ok((Instruction::RET, rest)),
            _ => Err(Error{
                kind: ErrorKind::BadInstruction(String::from(instruction)),
                underlying: None
            })
        }
    }
}

impl Instruction {
    /// Compile an assembly-time [Instruction] into a fully-compiled [runtime::Instruction]
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<runtime::Instruction, Error> {
        match self {
            Instruction::LOD(r, v) => v.compile(labels).map(|v_compiled| runtime::Instruction::LOD(*r, v_compiled)),
            Instruction::STR(r, m) => m.compile(labels).map(|m_compiled| runtime::Instruction::STR(*r, m_compiled)),
            Instruction::BRZ(r, m) => m.compile(labels).map(|m_compiled| runtime::Instruction::BRZ(*r, m_compiled)),
            Instruction::BNZ(r, m) => m.compile(labels).map(|m_compiled| runtime::Instruction::BNZ(*r, m_compiled)),
            Instruction::ADD(r, v) => v.compile(labels).map(|v_compiled| runtime::Instruction::ADD(*r, v_compiled)),
            Instruction::MUL(r, v) => v.compile(labels).map(|v_compiled| runtime::Instruction::MUL(*r, v_compiled)),
            Instruction::SUB(r, v) => v.compile(labels).map(|v_compiled| runtime::Instruction::SUB(*r, v_compiled)),
            Instruction::DIV(r, v) => v.compile(labels).map(|v_compiled| runtime::Instruction::DIV(*r, v_compiled)),
            Instruction::JMP(m) => m.compile(labels).map(|m_compiled| runtime::Instruction::JMP(m_compiled)),
            Instruction::CAL(m) => m.compile(labels).map(|m_compiled| runtime::Instruction::CAL(m_compiled)),
            Instruction::MVF(r) => Ok(runtime::Instruction::MVF(*r)),
            Instruction::PSH(r) => Ok(runtime::Instruction::PSH(*r)),
            Instruction::POP(r) => Ok(runtime::Instruction::POP(*r)),
            Instruction::RET => Ok(runtime::Instruction::RET),
            Instruction::HLT => Ok(runtime::Instruction::HLT),
        }
    }
}