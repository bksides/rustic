use std::collections::HashMap;

use crate::assembly::{Error, ErrorKind};

#[derive(Debug)]
/// A compile-time stand-in for a u16 value, which is later compiled to a u16
pub enum IndexedValue {
    /// Represents a value that has an index component; note that indices are forbidden *inside* indices,
    /// because the value of the index is not itself an IndexedValue
    Indexed(Box<IndexedValue>, Value),
    /// Represents a value that has no index component; i.e. a [Value]
    NonIndexed(Value)
}

#[derive(Debug)]
/// A non-indexed value; used when parsing array indices, which cannot be nested with further indices;
/// as well as as a base case for parsing the non-indexed part of an [IndexedValue].
pub enum Value {
    /// Represents a [Literal] u16 value
    Literal(Literal),
    /// Represents a [Label] to be compiled to a u16 value
    Label(Label)
}

#[derive(Debug)]
/// A label referencing a u16 value
pub struct Label(pub String);

#[derive(Debug)]
/// A literal u16 value, written in hex (`0x`) or decimal
pub struct Literal (pub u16);

impl Label {
    /// Read a label from the given token and return the place in the token where the label ends
    pub fn read_from(token: &str) -> Result<(Label, &str), Error> {
        let mut chariter = token.chars();
        chariter.next().ok_or(Error{
            kind: ErrorKind::LabelExpected,
            underlying: None
        }).and_then(|c| match ('a'..'z').contains(&c) || ('A'..'Z').contains(&c) || c == '_' {
            true => Ok(c),
            false => Err(Error{
                kind: ErrorKind::BadLabel(String::from(token)),
                underlying: None
            })
        })
        .map(|c| {
            let label = format!("{}{}", c, (&mut chariter).collect::<String>());
            (Label(label), chariter.as_str())
        })
    }
}

impl Literal {
    /// Read a literal from the given token and return the place in the token where the label ends
    pub fn read_from(token: &str) -> Result<(Literal, &str), Error> {
        let base16 = token.starts_with("0x");
        let base = if base16 {16} else {10};
        let start = if base16 {&token[2..]} else {token};
        u16::from_str_radix(start, base).map_err(|err| Error{
            kind: ErrorKind::BadLiteral(String::from(start)),
            underlying: Some(Box::new(err))
        }).map(|l| (Literal(l), token))
    }
}

impl IndexedValue {
    fn parse_indices(val: IndexedValue, rest: &str) -> Result<(IndexedValue, &str), Error> {
        let mut chariter = rest.chars();
        match chariter.next() {
            Some('[') => Value::read_from(chariter.as_str())
                .and_then(|(ind, rest)| match rest.chars().next() {
                    Some(']') => Ok((IndexedValue::Indexed(Box::new(val), ind), &rest[1..])),
                    _ => Err(Error{
                        kind: ErrorKind::BadIndexedValue(String::from(rest)),
                        underlying: None
                    })
                }
            ),
            _ => Ok((val,rest))
        }
    }

    /// Compile the given IndexedValue into a final u16 value
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<u16, Error> {
        match self {
            IndexedValue::Indexed(val, idx) => val.compile(labels)
                .and_then(|compiled_val| idx.compile(labels)
                    .map(|compiled_idx| compiled_val + compiled_idx)),
            IndexedValue::NonIndexed(val) => val.compile(labels)
        }
    }

    /// Read an IndexedValue from the given &[str]
    pub fn read_from(token: &str) -> Result<(IndexedValue, &str), Error> {
        let token = token.trim();
        match token.chars().next() {
            None => Err(Error{
                kind: ErrorKind::IndexedValueExpected,
                underlying: None
            }),
            Some(_) => Value::read_from(token)
                .map(|(value, rest)| (IndexedValue::NonIndexed(value), rest))
        }.and_then(|(val, rest)| IndexedValue::parse_indices(val, rest))
    }
}

impl Value {
    /// Compile the given value into a final u16 value
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<u16, Error> {
        match self {
            Value::Literal(Literal(literal)) => Ok(*literal),
            Value::Label(Label(label)) => labels.get(label).ok_or(Error{
                kind: ErrorKind::UnresolvedLabel(String::from(label)),
                underlying: None
            }).map(|compiled| *compiled)
        }
    }

    /// Read a Value from the given &[str]
    pub fn read_from(token: &str) -> Result<(Value, &str),  Error> {
        Literal::read_from(token).map(|(l,r)| (Value::Literal(l), r))
        .or_else(|_| Label::read_from(token).map(|(l,r)| (Value::Label(l), r)))
    }
}