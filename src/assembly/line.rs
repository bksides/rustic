use std::collections::HashMap;
use std::iter::Peekable;

use crate::assembly::{Instruction, Error, ErrorKind, Label, Token, Resolvable};
use crate::lexer;
use crate::common::InstructionType;

#[derive(Debug)]
/// Represents a single line of a RUSTIC assembly program
pub enum Line {
    /// Represents an instruction line, which encodes an [Instruction] to be executed
    Instruction(Instruction),
    /// Represents a data line, which encodes [Data] to be inserted into the program
    Data(Data),
    /// Represents a [LabelDeclaration], which associates the given label with the currently location in memory for later resolution
    LabelDeclaration(LabelDeclaration),
    /// Represents an empty line
    Empty,
}

impl lexer::SyntaxNode for Line {
    type Token = Token;
    type Error = Error;

    fn read_from<I: Iterator<Item=Token>>(iter: Peekable<I>) -> Result<Self, Self::Error> {
        match iter.peek() {
            Some(Token::Instruction(i)) => Instruction::read_from(iter)
                .map(|instruction| Line::Instruction(instruction)),
            Some(Token::Label(l))       => LabelDeclaration::read_from(iter)
                .map(|label_declaration| Line::LabelDeclaration(label_declaration)),
            Some(Token::Number(n))      => Data::read_from(iter)
                .map(|data| Line::Data(Data(data))),
            Some(t)                     => Err(Error{
                kind: ErrorKind::UnexpectedToken(t),
                underlying: None
            }),
            None                        => Err(Error{
                kind: ErrorKind::TokenExpected,
                underlying: None
            })
        }
    }
}

impl Line {
    /// Compiles a line into raw executable bytes
    pub fn compile(&self, labels: &HashMap<String, u16>) -> Result<Vec<u8>, Error> {
        match self {
            Line::Instruction(instruction) => instruction.compile(labels).map(|runtime_instruction| runtime_instruction.into()),
            Line::Data(data) => data.compile(labels),
            Line::LabelDeclaration(_) => Ok(Vec::new()),
            Line::Empty => Ok(Vec::new())
        }
    }
}

#[derive(Debug)]
/// Syntactic element denoting the declaration of a given [Label]
pub struct LabelDeclaration(pub Label);

impl LabelDeclaration {
    /// Read a LabelDeclaration from the given &[str]
    pub fn read_from(line: &str) -> Result<(LabelDeclaration, &str), Error> {
        Label::read_from(line)
        .and_then(|(label, rest)| match rest.chars().next() {
            Some(':') => Ok((LabelDeclaration(label), rest)),
            _ => Err(Error{
                kind: ErrorKind::BadLabelDeclaration(format!("{};{}", label.0, rest)),
                underlying: None
            })
        })
    }
}

#[derive(Debug)]
/// Sytactic element denoting an arrangement of [ByteLiteral]s
pub struct Data(pub Vec<ByteLiteral>);

impl Data {
    /// Compile this [Data] element into bytes to insert into the final executable
    pub fn compile(&self, _: &HashMap<String, u16>) -> Result<Vec<u8>, Error> {
        Ok(self.0.iter().map(|b| b.0).collect::<Vec<u8>>())
    }

    /// Read a Data element from the given &[str]
    pub fn read_from(line: &str) -> Result<(Data, &str), Error> {
        ByteLiteral::read_from(line)
        .and_then(|(byte_literal, rest)| match Data::read_from(rest.trim()) {
            Ok((Data(mut v), rest)) => {
                v.insert(0, byte_literal);
                Ok((Data(v), rest))
            }
            Err(Error{
                kind: ErrorKind::ByteLiteralExpected,
                underlying: _
            }) => Ok((Data(vec![byte_literal]), rest)),
            Err(e) => Err(Error{
                kind: ErrorKind::BadData(String::from(rest)),
                underlying: Some(Box::new(e))
            })
        })
    }
}

#[derive(Debug, Copy, Clone)]
/// A syntactic element denoting a byte to be inserted directly into the program
pub struct ByteLiteral(u8);

impl ByteLiteral {
    /// compile this [ByteLiteral] element into bytes to insert into the final executable
    pub fn compile(&self, _: &HashMap<String, u16>) -> Result<Vec<u8>, Error> {
        Ok(vec![self.0])
    }
    
    /// Read a ByteLiteral element from the given &[str]
    pub fn read_from(token: &str) -> Result<(ByteLiteral, &str), Error> {
        let base16 = token.starts_with("0x");
        let base = if base16 {16} else {10};
        let start = if base16 {&token[2..]} else {token};
        u8::from_str_radix(start, base).map_err(|err| Error{
            kind: ErrorKind::BadLiteral(String::from(start)),
            underlying: Some(Box::new(err))
        }).map(|l| (ByteLiteral(l), token))
    }
}