// rustic-assembler.rs
// Author: Technologenesis
/*
The RUSTIC assembler facilitates writing cartridges for the RUSTIC platform
by allowing developers to write in terms of named instructions, registers,
and memory locations rather than directly composing programs byte-by-byte.
*/

// Usage: rustic-assembler <source file> <dest file>

use rustic::assembly::Label;
use rustic::assembly::LabelDeclaration;
use rustic::assembly::Data;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader, Write};
use std::collections::HashMap;
use rustic::assembly::Line;

fn main() {
    let args: Vec<String> = env::args().collect();
    let source = args.get(1).unwrap_or_else(|| {
        panic!("no source file provided");
    });
    let dest = args.get(2).unwrap_or_else(|| {
        panic!("no dest file provided");
    });

    let file = File::open(source).unwrap_or_else(|e| {
        panic!("the source file could not be opened: {}", e);
    });

    let file_reader = BufReader::new(file);

    let mut labels = HashMap::<String, u16>::new();
    let mut lines = Vec::<Line>::new();
    let mut mem_counter: u16 = 0;
    let mut line_num = 0;

    for line_result in file_reader.lines() {
        let line = line_result.unwrap_or_else(|_| {panic!("failed to read line from source file")});
        line_num += 1;
        let parse_result = Line::read_from(line.as_str()).map(|(parsed_line, _)| parsed_line);
        match parse_result {
            Err(err) => {
                println!("Error at line {} \"{}\": {}", line_num, line, err.to_string());
                return;
            },
            Ok(parsed_line) => match &parsed_line {
                Line::Instruction(instruction) => {
                    mem_counter += u16::from((&instruction).bytes_taken());
                    lines.push(parsed_line);
                },
                Line::LabelDeclaration(LabelDeclaration(Label(label))) => {labels.insert(String::from(label), mem_counter);},
                Line::Data(Data(data)) => {
                    mem_counter += data.len() as u16;
                    lines.push(parsed_line);
                },
                Line::Empty => ()
            }
        }
    }

    let mut file = File::create(dest).unwrap_or_else(|e| {
        panic!("the dest file could not be opened: {}", e);
    });
    for parsed_line in lines {
        match parsed_line.compile(&labels) {
            Ok(bytes) => if let Err(e) = file.write(&bytes) {
                println!("{}", e.to_string());
                return;
            },
            Err(err) => {
                println!("{}", err.to_string());
                return;
            }
        }
    }
}