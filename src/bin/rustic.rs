use std::io;
use std::fs::File;
use std::io::{BufReader, BufRead, Write, Read};
use regex::Regex;

use rustic::runtime;
use rustic::assembly;
use rustic::common::{A_16, B_16, C_16, D_16};

fn main() {
    println!("   ___  __  _____________________   _______\n  / _ \\/ / / / __/_  __/  _/ ___/__<  / __/\n / , _/ /_/ /\\ \\  / / _/ // /__/___/ / _ \\\n/_/|_|\\____/___/ /_/ /___/\\___/   /_/\\___/");
    let reader = BufReader::new(io::stdin());
    let mut machine = runtime::RUSTIC::new();
    print!("> ");
    io::stdout().flush().unwrap();
    for line_res in reader.lines() {
        let line = line_res.unwrap();
        let mut words = line.split_ascii_whitespace();
        if let Some(word) = words.next() {
            match word {
                "cycle" => if let Err(e) = machine.cycle() {println!("{}", e.to_string())},
                "register_a"  => println!("{}", machine.at_register(A_16)),
                "register_b"  => println!("{}", machine.at_register(B_16)),
                "register_c"  => println!("{}", machine.at_register(C_16)),
                "register_d"  => println!("{}", machine.at_register(D_16)),
                "flag"        => println!("{}", machine.flag()),
                "instruction" => println!("{}", machine.instruction_ptr()),
                "help" => println!("{}","- `help`: shows this help text\n\
                - `cycle`: execute one cycle on this machine, printing the executed instruction (or the error, if one occurred)\n\
                - `register_a`: print the contents of register A_16 (i.e., all 16 bits of register A)\n\
                - `register_b`: print the contents of register B_16\n\
                - `register_c`: print the contents of register C_16\n\
                - `register_d`: print the contents of register D_16\n\
                - `flag`: print the flag value (representing the error state of the last instruction)\n\
                - `instruction`: print the current value of the instruction pointer\n\
                - `memory {{memloc}}`, where `memloc` is a decimal or hex (starting with `0x`) memory address: print the byte at the given address\n\
                - `load {{filename}}`, where `filename` is the path to a binary `.rstc` file: load the given binary into memory for execution\n\
                - `run`: cycle the machine until it halts\n\
                - `reset`: restore the machine to its original state"),
                "memory" => {
                    match words.next().map(|word| parse_literal(word).map_err(|err| assembly::Error{
                        kind: assembly::ErrorKind::BadLiteral(String::from(word)),
                        underlying: Some(Box::new(err))
                    })) {
                        None => println!("expected address"),
                        Some(result) => match result {
                            Ok(addr) => match machine.fetch_from_addr(addr) {
                                Ok(val) => println!("{}", val),
                                Err(err) => println!("{}", err.to_string())
                            },
                            Err(err) => println!("{}", err.to_string())
                        }
                    }
                },
                "load" => match words.next().map(|word| File::open(word)) {
                    None => println!("expected file"),
                    Some(result) => match result {
                        Ok(result) => machine.load_into_memory(result.bytes().map(|elem_res| elem_res.unwrap())),
                        Err(err) => println!("{}", err)
                    }
                }
                "run" => while !machine.is_halted() {
                    if let Err(e) = machine.cycle() {
                        println!("RuntimeError: {}", e.to_string());
                    };
                }
                "reset" => machine = runtime::RUSTIC::new(),
                _ => ()
            }
        }
        print!("> ");
        io::stdout().flush().unwrap();
    }
}

fn parse_literal(literal: &str) -> Result<u16, assembly::Error> {
    let literal_pattern = Regex::new("^(0x)?([A-Fa-f\\d]+)$").unwrap();
    literal_pattern.captures(literal).ok_or(assembly::Error{
        kind: assembly::ErrorKind::BadLiteral(String::from(literal)),
        underlying: None
    }).and_then(|captures| {
        let base16 = captures.get(1).is_some();
        captures.get(2).ok_or(assembly::Error{
            kind: assembly::ErrorKind::BadLiteral(String::from(literal)),
            underlying: None
        }).map(|capture| (capture, base16))
    }).and_then(|(capture, base16)| u16::from_str_radix(capture.as_str(), if base16 {16} else {10}).map_err(|err| assembly::Error{
        kind: assembly::ErrorKind::BadLiteral(String::from(literal)),
        underlying: Some(Box::new(err))
    }))
}

